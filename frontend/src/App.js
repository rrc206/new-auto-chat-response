import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Redux from "./pages/Redux";
import ApiCall from "./pages/ApiCall";
import { site_text } from "./utils/languageMapper";
import { useDispatch, useSelector } from "react-redux";
import { updateLanguage } from "./redux/slices/config/configSlice";
import pwcwhite from "./assets/images/pwcwhite.png";
import HomeIcon from "@mui/icons-material/Home";
import PasteIcon from '@mui/icons-material/ContentPaste';
import PersonIcon from '@mui/icons-material/Person';
import { size } from "lodash";
import LogoutIcon from '@mui/icons-material/Logout';

function App() {
  const [showContacts, setShowContacts] = useState(false);

  const config = useSelector((state) => state.config);
  const dispatch = useDispatch();

  window.site_lang = config?.language;
  window.site_text = site_text;

  React.useEffect(() => {
    const lang_value = localStorage.getItem("site-lang");
    if (lang_value) {
      dispatch(updateLanguage(lang_value));
    } else {
      dispatch(updateLanguage("English"));
    }
  }, []);

  const changeLang = (lang) => {
    dispatch(updateLanguage(lang));
    localStorage.setItem("site-lang", lang);
  };

  return (
    <div>
      <div className="main-div" style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
        <div className="l-panel d-flex justify-content-start">
          <div className="logo">
            <img src={pwcwhite} alt="" />
          </div>
          <ul className="page-nav w-100">
            <li className="active">
              <HomeIcon />
            </li>
            <li className="active" style={{ marginTop: "18px" }}>
              <PasteIcon />
            </li>
            <li className="active" style={{ marginTop: "18px" }}>
              <PersonIcon />
            </li>
            <li className="active" style={{ marginTop: "350px" }}>
              <LogoutIcon/>
            </li>
          </ul>
        </div>
        <>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/redux" element={<Redux />} />
            <Route exact path="/api" element={<ApiCall />} />
          </Routes>
        </>

        <div style={{ position: "fixed",zIndex:"11", top: "0", right: "0", width: "83px", height: "100%", backgroundColor: "rgb(236,236,236)", marginLeft: "-64px", display: "flex", flexDirection: "column" }}>
          <ul style={{ alignItems: "center", display: "flex", flexDirection: "column", rowGap: "10px", columnGap: "10px", paddingTop: "0px", paddingRight: "5px", paddingBottom: "0px", paddingLeft: "5px" }}>
            <li>
              <div style={{ marginTop: "28px", paddingLeft: "2px" }}>
                <span
                  id="1"
                  onMouseOver={() => {
                    setShowContacts(true);
                    document.getElementById("1").style.backgroundColor = "white";
                    document.getElementById("1-button").style.backgroundColor = "white";
                  }}
                  onMouseOut={() => {
                    setShowContacts(false);
                    document.getElementById("1").style.backgroundColor = "transparent";
                    document.getElementById("1-button").style.backgroundColor = "rgb(236,236,236)";
                  }}
                  style={{
                    width: "100%",
                    paddingTop: "20px",
                    paddingRight: "0px",
                    paddingBottom: "10px",
                    paddingLeft: "0px",
                    transitionDuration: "0.2s",
                    transitionTimingFunction: "ease",
                    transitionDelay: "0s",
                    transitionProperty: "all",
                    backgroundColor: "transparent",
                  }}
                >
                  <button id="1-button" style={{ height: "50px", backgroundColor: "rgb(236,236,236)", borderColor: "rgb(236,236,236)", borderWidth: "0px" }}>
                    <img style={{ paddingTop: "4px" }} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABHNCSVQICAgIfAhkiAAAAhZJREFUWEftmEFOAjEUhtvhAGqCbmVtAhlPoG4grvQGwgnkBuIJxBMIN8CVgY16AgkkrsetkoAHmKmvZCbGoZ15b1oGAjaZQKav7Tf/a19fy5miuE/Tku/4d1DlwlNS2eTwzhOBaPD4QCHcG7zfzQEibQhvAbDcn/Sg1UVay7zqVYDTNVFvroEKUOSlDmacrQPs+jxov1cPhlKdo8GnWxBOE/5eYdRS2dhS8BvATiOw+EAh6Au836GCWgEEuGMdXAQUQsrwRSo2ALvjWrGOGRVCWIfqbg6NjFYtRj0TFY0BQb0FLySpSRVk8wGX7uK4O6gugPbLXSQWABlGxVWGGfmNM4A8SwnUz1mSEBtx8NcJgnV8J7j/s9UFzjWkJKg4qdzqMsw5TEy2ZmMcZqyRaDraOsBXEALOEcKTggjGS/Ajn5OsSttYJF0YvAdbnjzLaAvM9UuolA8pNzQBfCwEhebwfG+uFraEp8Y22KMOZtkAOWuMq8UOFkplVx5M6jAHHtL6oANagIugMJCkVQyT/3ZU22+lfTWlvtL/asFiutG1QQNyzj5G1aJckdZLZTDxhGCHqo7RgLBdGc873ZcluRoNiMlYskqblOmgFwk1tafC6nKCzQGkKmLLHq2grQGp/fwDUhWL26sUnIER+ZLHFES7k8Qr1v4KOEyH5P3eylWU26vyXiXK2cDA1e2Ry3Jp1K+EC3xR/wFVSyAdgHqh7QAAAABJRU5ErkJggg=="></img>
                    <p style={{ color: "black", marginTop: "7px", fontSize: "12px" }}><b>{showContacts ? "contacts" : ""}</b></p>
                  </button>
                </span>
              </div>
            </li>
            <br></br>
            <br></br>
            <li> 
              <div style={{ marginTop: "-18px", paddingLeft: "2px" }}>
                <span
                  id="2"
                  onMouseOver={() => {
                    setShowContacts(true);
                    document.getElementById("2").style.backgroundColor = "white";
                    document.getElementById("2-button").style.backgroundColor = "white";
                  }}
                  onMouseOut={() => {
                    setShowContacts(false);
                    document.getElementById("2").style.backgroundColor = "transparent";
                    document.getElementById("2-button").style.backgroundColor = "rgb(236,236,236)";
                  }}
                  style={{
                    width: "100%",
                    paddingTop: "20px",
                    paddingRight: "0px",
                    paddingBottom: "10px",
                    paddingLeft: "0px",
                    transitionDuration: "0.2s",
                    transitionTimingFunction: "ease",
                    transitionDelay: "0s",
                    transitionProperty: "all",
                    backgroundColor: "transparent",
                  }}
                >
                  <button id="2-button" style={{ height: "50px", backgroundColor: "rgb(236,236,236)", borderColor: "rgb(236,236,236)", borderWidth: "0px" }}>
                    <img style={{ paddingTop: "3px" }} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAABHNCSVQICAgIfAhkiAAAAd5JREFUWEftl/1Nw0AMxdsJYAPCBjABYQJgArIBZQIYASYgnQCYgDABbEDYgE4A74fuaBTlw76WPxCxZF2r+OP5xbnzzWfpsivXC+mp9CCEedX6IL2VfqSEnqc4ySeX3ksB1SWAOZNW3vgpgGDjxZjoUHawZpYUQG+Knhkz1LLbN9p+m3kBFfK58yQIr46+MokXUKmo56bIa6OlflKISbyAKkU9MkVeGz3rZ271+fOArlXplbXaYHep9cbq42UoU2C+Mo/wldVWBy8g4lItO7RF2LEXFsNokwII30o61tyuZt4UEP70E9XvtBhYBRZ57hYY4jxib2HlDIpbPRVapOtwtfhFhjmKYu4lgJ6keU+ECJBXBFBAek9xkpGcHCSPALpSVgD6tJTTsAFcKX0cAAeIE2kxUGxn2hRAMRBM8cU1Z584I9FbfaPJYP2bAIqBYew4/OEQhZlk2QYgksftA3Bj28GvMzQBsvTD9MrGWJoYmhgaY2Ds+dRD/5shxoj2GDrGSPv5tnpoRSBGUOaaPS+Khv02AL0r3qLr1pGHRIyamTSOnUMsWgAx/DMGo3VYScXI8iOeaxDAAIu2h7A+QIy5JETj5WHwRXgANQMxnjKmFiEZK1IGwKy0gfdCMPsCZDVrJAS7tYgAAAAASUVORK5CYII="></img> {/* Empty src attribute */}
                    <p style={{ color: "black", marginTop: "15px", fontSize: "12px" }}><b>{showContacts ? "Knowledge" : ""}</b></p>
                  </button>
                </span>
              </div>
            </li>
            <br></br>
            <br></br>
            <li> 
              <div style={{ marginTop: "-18px", paddingLeft: "2px" }}>
                <span
                  id="3"
                  onMouseOver={() => {
                    setShowContacts(true);
                    document.getElementById("3").style.backgroundColor = "white";
                    document.getElementById("3-button").style.backgroundColor = "white";
                  }}
                  onMouseOut={() => {
                    setShowContacts(false);
                    document.getElementById("3").style.backgroundColor = "transparent";
                    document.getElementById("3-button").style.backgroundColor = "rgb(236,236,236)";
                  }}
                  style={{
                    width: "100%",
                    paddingTop: "20px",
                    paddingRight: "0px",
                    paddingBottom: "10px",
                    paddingLeft: "0px",
                    transitionDuration: "0.2s",
                    transitionTimingFunction: "ease",
                    transitionDelay: "0s",
                    transitionProperty: "all",
                    backgroundColor: "transparent",
                  }}
                >
                  <button id="3-button" style={{ height: "50px", backgroundColor: "rgb(236,236,236)", borderColor: "rgb(236,236,236)", borderWidth: "0px" }}>
                    <img style={{ paddingTop: "3px" }} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAgCAYAAACGhPFEAAAABHNCSVQICAgIfAhkiAAABA5JREFUWEfVWE1SGkEUfg3oNt4guBAqK/EE4gnEEwgHEOQEwROAeIDACcQTCCcQVilxIZ4g7p2Zzvd1DwLjkJlBSEhXUVTPvPf6e/+vR0nUun46FSVlUTorWgogf8VvKEr1xXG6Uv82iRKx7vdqqcDmuCBpucX7bMShLXF3rqS+T2VEms97knk7h4Il7Io+LxRTQ9FeT2r57meVCAfdHpch+Eds4QqWd3ZOJOMURGvyLVfU0EpF6rlhbPkBwo+grYXvQbeXUCjDJMorU5Gv4srJqsA/gm6PCXjq1oS4E5FPEFZHknKOJYVcMfmiaagJcgjecO+W5csi6JunItxL0Nuy+vBIPeiRAOhxBxqfbwviORwVqeY6030Q9INf1rYQt6pL9aBFYIug22O9hWhnkJQ6kYuD/v8Fmklaze0HQfeh1vF2W1ufBUA/XaLsNGOAHoHmMAYdSUjLFZf+z2KVdGeg2X7TbwRcjgTD2IpVGvUdsgYVSdUgsxgpNx7BwIK288L9rHLoF+RoAz8Ue+/SDEeie+KpIkDUsPdBA5SonuHjcyHIFDJcl80zFwZImyGrBSVbeIf5BPK4NPhEv0IW9jhLeRPwsDqgvZt3mF3UaZgeFnSwC2p9ZQArIRP/KRCHo+xQmMIh2uuI6xUlnX7ABFgBiIa4bgl7TIDyDPos9kfYcxbpGv4U8wUytAZIGgWhqOUa+yz230E3sLLR4FwBLwwl6msQuJKwLsg26oHZg8XJ6KqSZMyBo/cDebC7A5COjVUFS9JCDqydhkcIyBwsUMp4hlY8RPYrGKmPfd8ANe9gAAtuAEXhnYwFSpl2HJ5fIwh4pDZBNwx8KnbIgnhuCxYDnXEfZmtjjVvfkjgQbqYMJIlvYRz8Tg9L07oePYYwQvhQ4Slo4z0TLxxl9/yQMk0E62MlM4nYHv8yxIsLQ4sCYDPAlPGzQ7+RrRmTVIDPLd38c6UQSpqx/yIeLgwm1s1CTKtXnx9KgneWnB28I99UJj3Bs238L64KQa+/C9LiDkDGu0SE4Fr2COFazWc3BRq3FOO9bAJE0aRKn8lFvmeTYtu7oAk/VJlazoQLQMfugtGW2BTFHGAL2nZCJsWXTZ25ulzEsEqVOdnNy7DN5eaxhObAm/e/WpxPWFHs0qhWKdRxxG8YoNnskfQGvj71Bmg4xSTiwu6IDQj4S+Mpuqe7W37/ZhITefh3j+bPrGQyWQw3KFsYctjBVlbEDFWBjusPZHP3vph4DdnyL0zzUla9pXPwquUbdorEhxwuJzNMatmgQvFAk+sm8U19hFgNDjtJDLqUNj7oZMBHmACLn7XoMtTJQFOKrTL8hSQrYlXjpuLttjYFOH5Mh6nMb34ZDO8ex01MaPwFmsBaYiFEyG8yqasQxR9r3gAAAABJRU5ErkJggg=="></img> {/* Empty src attribute */}
                    <p style={{ color: "black", marginTop: "15px", fontSize: "12px" }}><b>{showContacts ? "Create Lead" : ""}</b></p>
                  </button>
                </span>
              </div>
            </li>
            <br></br>
            <br></br>
            <li> 
              <div style={{ marginTop: "-18px", paddingLeft: "2px" }}>
                <span
                  id="4"
                  onMouseOver={() => {
                    setShowContacts(true);
                    document.getElementById("4").style.backgroundColor = "white";
                    document.getElementById("4-button").style.backgroundColor = "white";
                  }}
                  onMouseOut={() => {
                    setShowContacts(false);
                    document.getElementById("4").style.backgroundColor = "transparent";
                    document.getElementById("4-button").style.backgroundColor = "rgb(236,236,236)";
                  }}
                  style={{
                    width: "100%",
                    paddingTop: "20px",
                    paddingRight: "0px",
                    paddingBottom: "10px",
                    paddingLeft: "0px",
                    transitionDuration: "0.2s",
                    transitionTimingFunction: "ease",
                    transitionDelay: "0s",
                    transitionProperty: "all",
                    backgroundColor: "transparent",
                  }}
                >
                  <button id="4-button" style={{ height: "50px", backgroundColor: "rgb(236,236,236)", borderColor: "rgb(236,236,236)", borderWidth: "0px" }}>
                    <img style={{ paddingTop: "3px",width:"60px" }} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFsAAAAzCAYAAAD4kXSuAAAABHNCSVQICAgIfAhkiAAACHdJREFUeF7tXHtYTGkcnpkuVlSPIl0m1KBUugipbEOl62SFWBLt0m5y39AIK7V0YcmudtWKNYrWVioZQnQRsek6Sppqn6RGJV0RavYcnvbZ7Wku3znfObX76J+envO+7+/9veec73zznW+iUj7+kJYAlbRKHwtRhnvYMmafL/KmytK0RyiOkmfYWplS+ijawt7eiRQarY4mS6vn38or7GnvftPX21tffD6Vg5zTN8P1vA67sJW1tRmMJc5ehnPN18krjNQCDe7d6zeCZzn3o++nX73Qyq8rB+UTiR82YU9xnL/E0NXuG9XJE6xgNdwpaC4pT7t+mJdyNQ6WJh6dIQ97mo2Vjd5yl0hVnQlmeBoRx33d3lGbdfTkpqf3iy8TVUMa3aEMe8SCIH/OhNkmy6QxCgPTUFqRkRt52qtLIGiGoQeqMSRhaxrpW9l/uzVBbrSCNqhhvHjkQdp9bfehz56Wlmfi1QLlkx72ROs5zva7/bigRmHji+JTthXGJ0fC1hWnR2rYRu6OvhY+nj+T2aC4WtW37kRnHTrhS5Yf0sI2dFuwYs56r3NkNSZtnarMvJ9yvo/eIC0eD46UsLWMDeycwtg38BglkpsbedL78bWcM0TWQLUJD3usru7UhT/uK6NSqfJEN4NHn8sOs2ksLc/FoyGJS3jY7lHf3VXRmTBHkpGhPt7T2Vkft3yDDuLjHVFeCA3bdMXCneZeS8OJMg9bty6/8Mz14Ehv2Lr9eoSFrUSnq3jEHEQ/PNDwmm+vF1RVZ+envevpaW8pr8p60dBUN1pdday2qZE1VVZGRcvc2EFNT9cSbx2Un7Y9eEZzOb8IhtZADcLCZvp/HT3ZzvorPKYrM7Jiy5LTQ9ufNFVL0lHU0lI1/Mx+lyHLzl8SVtzxhpLya1d2hTni0RDFJSZsFRWltXGR7VgNNz+uKboTHbeupYJfCKqhrK3GmLtx3Wn16fqfgnL78Vz/YPNGDLUl1SMk7BkrFwWZrVq8T1LxwY4LeJVZl3cesEOO9WHh93OcQnakaplPX4hFg38j75fsI9G47srB6hIS9pfpv3ZQaTRF0EYbSx/lcNkHmaA8UXjnMDZX09jAGYOeMNZlNe5nDeFjtpo+w8rtyL480AbfvnrVxFny9WSE1wnKFYf3OHmoTElzvBGoZsGp331LEi9Fg/LE4aFf2TNWLQ4yW7kIeAi5GXHCozbrTiLM5lAtZOxmuoYHZoHq1uT+8cOt0B+3gPJIDXtVQlT+CCVFCxCTrdV1vIub9kwH4YBgnQ7svKplZgQ0w0CmmZ1n3H2UQOpIwkK/stdeOSukCIWS6v7reAEn6ZuShNSjQCQA8ETrWYvtd29KAqC8hyLjtizyqxeUJwoPNWw6nT7SMebgS1BzSFNyCIewj8mon7VcDtgVgHBuRUTPqsnKKwDth5SwGQ42tvO2rgN6A/KyraP5/MqNarAaEqXjEROerkTXcAWpUxx/cd+D+IvBIBzSxmwsD8f60ofHMtjhW2E1JErHzNN9/wxP929B6hTFJe8vPJcSBMIZ1mEXnUvZXxiXDK0hUc1i+aD1/wsb8tXzMWwx9xaPey3k3vE4oNsby239v7uyjdydNlv4rDwGEsaLuobEZF+2BwgHC9Z+7+bQiZYz2SBc2EMc1KnfNMdPNay2+DSANCSkUIWnXLygr0MM9OCZECX4RElxPIi3O1Ecv4rLN6DtBoAaNtIIFZnPAq/WXTwQbNSax38IEgQgVgHx1Q3IoWQGHhnzZ3FxGyiPlHk2WuSLtFPdNFlZBRCDyIaZCGTDTAAIBwSLrNXsQaalISAcCpVKiXX2gnoxQhVDm3Hc73+cPssEbB+GkNIX67paGaF3AQUiHZjqnXrquYyc7Bjp4B9Q3S2ttxNWb8X8AmKwWtDDNl3O2mG+ZlkESGMotjb33rmboVGeoDxJeKvN3kHTnGyBVyFhPxxRn9DDRjRHIuMj8PoIauZKYIRDQzHvuqQApT2uaWIw1zmUjWkvSMqWEIPnVVUV0taSBkdE2BSn0IB0LRNDoHWID2aFfVd2hTORl663pTEvDjPOYLIZK3z3bZqMDNDzA9Xsanpe/Zv3NvRFBtQfQsJmMC3d5wWsT8bklEoVZuyNYNUXlGHe6apuojff5WAgF9mF9QkWDw84STuKE1IPY+GK4xASNlpwxdljfAXVMQyshp/8UZqUd/zMxu7mZgGAxqj5AX4/6DLnfAnA+TdUKOyJdV0zEr3NMGuIIBIWtgHLztPSbw3u77LUZN893cyvucpLyrggqvmpzrbO6vo6rCkLmH54Ayr49Xd2yYVLhOziIixstGm3o0H3kJ1Ks/EG0M8X9vZ1vX3VU9HW2ChQ1lRXk5OXM6TJyY2Gpf/25eunnKVf0WHpDdQhNOzxBlMsWIf35hNlHrZuOjvM/hmBX/8gNGw0DHPvZQGmy1hhsIJB9qP8LSUUIisDkEbWursP4q6HHPOC5XMwHcLDRou6hO3K1DCeZgvayKsX7bWPLmdyOp495/Ezc9HZzWDrLjTGvLkOyppjLXTnWS5RpmsAv6V/Vl51L317CLoxE9KpG7xTUsJGSssvPRmRi4yzUo3fyNbd80UXLn3f8qj6AegJGquno2e8lOWvYz3LRxpuT1d3Y7L/3pkvn7QArVZKo03qmP3PYqPGjVN3OxyYPWqc6lRRRpt4lfkFnMRNjbxK3G+0VSdN0rfwXREl7o7qffuujbszhNlUWVuKJTxQDllX9ntfaODM7T6JGtP1rQcafZiWsSf/RPwB0AYk4fVZ9j7WfqtjBuI6GpvKMkNPfN7K55P2/XZSw+5vWN+B6aagpmre//ej7JwYIm9jFWQL8yQnm23If3J4329Xa1v1Y+7Ns5JOFOzjQxI27Cb+K3ofwybxTP0Fb+PZUmPsU+oAAAAASUVORK5CYII="></img> {/* Empty src attribute */}
                    <p style={{ color: "black", marginTop: "15px", fontSize: "12px" }}><b>{showContacts ? "Service Now" : ""}</b></p>
                  </button>
                </span>
              </div>
            </li>
            <br></br>
            <br></br>
            <li>
              <div style={{ marginTop: "-18px", paddingLeft: "2px" }}>
                <span
                  id="5"
                  onMouseOver={() => {
                    setShowContacts(true);
                    document.getElementById("5").style.backgroundColor = "white";
                    document.getElementById("5-button").style.backgroundColor = "white";
                  }}
                  onMouseOut={() => {
                    setShowContacts(false);
                    document.getElementById("5").style.backgroundColor = "transparent";
                    document.getElementById("5-button").style.backgroundColor = "rgb(236,236,236)";
                  }}
                  style={{
                    width: "100%",
                    paddingTop: "20px",
                    paddingRight: "0px",
                    paddingBottom: "10px",
                    paddingLeft: "0px",
                    transitionDuration: "0.2s",
                    transitionTimingFunction: "ease",
                    transitionDelay: "0s",
                    transitionProperty: "all",
                    backgroundColor: "transparent",
                  }}
                >
                  <button id="5-button" style={{ height: "50px", backgroundColor: "rgb(236,236,236)", borderColor: "rgb(236,236,236)", borderWidth: "0px" }}>
                    <img style={{ paddingTop: "3px",width:"40px" }} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAArCAYAAADhXXHAAAAABHNCSVQICAgIfAhkiAAAA4JJREFUWEfNmI1x1DAQRrkKCBXgVECoAFMBoYIcFQAVYCoAKuCogFBBTAVABTgVECqAfUZ7s6fItlY+B+/MjuPTSnr6tPpxNvfy7UxCn4jX4pU479ZaeenEeX4Rv8lvOi9yMxF2IuXPxJsAmNfqv6hL8Q8B3lNvMHYM9lxqvRuAvA4q2oYreXmY6AmlXyTi3QNIwaImkFvT2u+gFGrhQ0bdWpyBXpggUuK1+M5NaCrEsHR2Ja75COSrwk5QuomgUbgYOIb9ZkBZJKg7d6GgNLNxP4hUDGxh30tjL0ODnwLonFmzdZkpFH0Ufnwsz+/exhWW0TP9GIqSc8c2gNugcCfPU28HCgsowORoJT536oc4yH8WL+ZOB2AZMbmKsWJJhyUNVdniSAPSIduAbcTfhBoPFlRVoay6pALwWQZsK84x+kM8PkKzGnEG2Zl0pQKwf0JnX+VZOzsuDdc+34aZzWpnTbCkB/vxYFr8D9hKgH4GKa2ymo6NlPH7LbM5S+HULSxruiaCtlL+MQGrqUER4M/FD7ZQ4JCeayDmSvhCct3Tqf40gFXyVLW1WbY2yvfAwNqRuvc+J3At8XpS2gOIE/Nzoq0DHp126PWisdTBEN/o7P1jJ/3bK6Xl3ue1wjZSqgfDUulAnjKLGKqy33biDOLXyAwhJIfHjV1QSK63omMCAwOovRzZtRELleLuZ9vCVvIDwJoOVGLxEYgCJVYHUNpWs9PP7/HCSvXTH1jxVmWvcbbSTl74+Mu9g5J/WzqIeqYNNn8szuEpMTapfZVGgEp9/HXyeyuuT+0AhXDgYkBiyFHgmSk1m8NToJQnYSkAiMvNXAOSKyduN3gvKBynQyfWHFhub8wMKlol6ZA0A5Sn11zKolIdQOiMdLGGckM5TSzfd+RrXC8Hmr5PcpW1oDmNawyDApJtqwRS2+m/C3Ngc0EBY9tDff7mOQfQisKl5nIKNgVqb0celUtj+xSg8hgsAUxfF/XSyvsxdopc+F7VMVgg45Vs8xBge9LlduyNs6dd8WWbwaSudF6YsXiOWPrZ789zvgxYREspfKCojmgOLG2Q15xOQ3fRUqWTXHNhFaaSP9jwmbbUncILvSishSE9rGsZs6D5pyed/tcyHsydwXpUJIVSwKuEZWC7RM6vFjYFvGpYgLfi+s+P1cMCXIe0YHe5ZcfaujyLairW7hoHsX8BbC+7TO7WWk4AAAAASUVORK5CYII="></img> {/* Empty src attribute */}
                    <p style={{ color: "black", marginTop: "15px", fontSize: "12px" }}><b>{showContacts ? "Chat Now" : ""}</b></p>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
